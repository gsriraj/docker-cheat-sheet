import './App.css';
import { CopyBlock, dracula } from "react-code-blocks";

const data = [
  {
    "heading": "Docker version",
    "script": `docker version`
  },
  {
    "heading": "Simple Nginx server",
    "script": `docker container run --publish 80:80 nginx
    docker container run --publish 80:80 --detach nginx
    docker container run --publish 80:80 --detach --name webhost nginx`
  },
  {
    "heading": "List container process status",
    "script": `docker container ls
    docker container ls -a`
  },
  {
    "heading": "stop container",
    "script": `docker container stop <container_id>`
  },
  {
    "heading": "logs container",
    "script": `docker container logs <container_name>`
  },
  {
    "heading": "get a list of docker commands",
    "script": `docker container --help`
  },
  {
    "heading": "remove container",
    "script": `docker container rm <container_id1 container_id2 ..>
    docker container rm $(docker container ls -aq) #remove all container`
  },
  {
    "heading": "list the process running in a specific contaner",
    "script": `docker top <container_name>`
  },
  {
    "heading": "Run container with a env var",
    "script": `docker container run --publish 3306:3306 --detach --name mysqldb --env MYSQL_RANDOM_ROOT_PASSWORD=yes mysql`
  },
  {
    "heading": "Inspect container and view its booting process",
    "script": `docker container inspect <container-name>`
  },
  {
    "heading": "View container status lke ram, cpu",
    "script": `docker container stats <container-name>
    docker contaier stats # get status on all running containers`
  },
  {
    "heading": "start new container interactively",
    "script": `docker container run -it --name webhost nginx bash
    exit # to exit interactive mode`
  },
  {
    "heading": "start a stopped container interactively",
    "script": `docker container start -ai webhost`
  },
  {
    "heading": "Run additional commads in existing container",
    "script": `docker container exec -it <container_name>`
  },
  {
    "heading": "Get port details of a running container",
    "script": `docker container port <container_name>`
  },
  {
    "heading": "Inspect a perticular propert of a running container",
    "script": `docker container inspect --format "{{ .NetworkSettings.IPAddress }}" <container_name>`
  },
  {
    "heading": "show docker networks",
    "script": `docker network ls`
  },
  {
    "heading": "inspect a network",
    "script": `docker network inspect <network-name>`
  },
  {
    "heading": "create a network",
    "script": `docker network create <network-name>
    docker network create <network-name> --driver`
  },
  {
    "heading": "Attach a network container",
    "script": `docker network connect <container_id>`
  },
  {
    "heading": "Detach a network from a container",
    "script": `docker network disconnect <container_id>`
  },
  {
    "heading": "Run with --rm to clean up",
    "script": `docker run --rm -it ubuntu:14.04 bash`
  },
  {
    "heading": "Docker network alias example",
    "script": `docker network create elastic
    docker container run -d --network elastic --network-alias search elasticsearch:2
    docker container run -d --network elastic --network-alias search elasticsearch:2
    docker container run --rm --net elastic alpine nslookup search
    docker container run --rm --net elastic centos curl -s search:9200
    docker container run --rm --net elastic centos curl -s search:9200`
  },
  {
    "heading": "docker image list",
    "script": `docker image ls`
  },
  {
    "heading": "docker pull images from docker hub",
    "script": `docker pull <image-name>`
  },
  {
    "heading": "show layers of changes made to an image",
    "script": `docker image history`
  },
  {
    "heading": "inspect image",
    "script": `docker image inspect`
  },
  {
    "heading": "Tag an image",
    "script": `docker image tag <source_image> <target_image>:<tag_name> # default tag_name is latest`
  },
  {
    "heading": "docker push image to docker hub",
    "script": `docker login
    docker image push <image_name>`
  },
  {
    "heading": "view login credentials of current docker user",
    "script": `cat .docker/config.json`
  },
  {
    "heading": "build an image from a dockerfile",
    "script": `docker image build --tag <image-name>  # takes the default docker file from the current directory
    use --file or -f to use custom docker file`
  },
  {
    "heading": "remove dangling images",
    "script": `docker image prune
    docker image prune -a`
  },
  {
    "heading": "clean up all images",
    "script": `docker system prune`
  },
  {
    "heading": "remove image",
    "script": `docker image rm <image-name>`
  },
  {
    "heading": "view docker space usage",
    "script": `docker system df`
  },
  {
    "heading": "view volume",
    "script": `docker volume ls`
  },
  {
    "heading": "remove unused volume",
    "script": `docker volume prune`
  },
  {
    "heading": "named volume example",
    "script": `docker container run -d --name mysql -e MYSQL_ALLOW_EMPTY_PASSWORD=True -v mysqldb:/var/lib/mysql mysql`
  },
  {
    "heading": "create docker volume",
    "script": `docker volume create <volume-name> # must be done before docker run view docker volume --help`
  },
]

function App() {
  return (
    <div className="App" style={{ marginLeft: '20rem', marginRight: '20rem' }}>
      <h2>Docker cheat sheet</h2>
      {
        data.map(data => {
          return (
            <div>
              <h3>{data.heading}</h3>
              <CopyBlock
                language="powershell"
                text={data.script}
                codeBlock
                theme={dracula}
                showLineNumbers={false}
                className="code-space"
              />
            </div>
          )
        })
      }
    </div>
  );
}

export default App;
